<?php

namespace phpoc\test;

define("PAYLOAD_STR", "ei2fanl63zr2s9x");
define("PAYLOAD_NB", "490308101");

define("TESTS_DIR", PHPOC_CWD . "tests/");
define("TESTS_SQLI_DIR", PHPOC_CWD ."../test_cases/sqli/");
define("TESTS_MISC_DIR", PHPOC_CWD ."../test_cases/misc/");

define("CONTROLLERS_DIR", PHPOC_CWD ."controllers/");
