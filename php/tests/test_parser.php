<?php

namespace phpoc\test;
use \phpoc\parser as parser;

require_once "common/include.php";

function test_gets_functions() {
    $functions = parser\functions_get(TESTS_SQLI_DIR . "simple.php");

    ensure(sizeof($functions) == 3);
    ensure($functions[0]->name == "dangerous");
    ensure($functions[1]->name == "safe");
    ensure($functions[2]->name == "nothing");

    ensure(strpos($functions[0]->get_body_str(), "mysql_query") !== FALSE);
    ensure(strpos($functions[1]->get_body_str(), "mysql_query") !== FALSE);
    ensure(strpos($functions[2]->get_body_str(), "mysql_query") === FALSE);
}

function test_execute_function() {
    $functions = parser\functions_get(TESTS_MISC_DIR . "simple.php");

    $add_one = $functions[0];
    $append_lol = $functions[1];

    ensure($add_one->name == "add_one");
    ensure($append_lol->name == "append_lol");

    ensure($add_one->exec(1) == 2);
    ensure($add_one->exec(4) == 5);

    ensure($append_lol->exec("awesome") == "awesome lol");
    ensure($append_lol->exec("great") == "great lol");
}

function test_replace_function_calls() {
    $orig_func_name = "mysql_query";
    $new_func_name = "whatever_query";
    $dangerous = parser\function_get(TESTS_SQLI_DIR . "simple.php", "dangerous");

    ensure($dangerous->name == "dangerous");
    ensure(strpos($dangerous->get_body_str(), $orig_func_name) !== FALSE);

    $dangerous->replace_function($orig_func_name, $new_func_name);

    ensure(strpos($dangerous->get_body_str(), $orig_func_name) === FALSE);
    ensure(strpos($dangerous->get_body_str(), $new_func_name) !== FALSE);
}

function test_mock() {
    $input = "sample input string";
    $call_with_input = parser\function_get(TESTS_MISC_DIR . "simple.php", "call_with_input");

    $mock = new \phpoc\PhpocMock('whatever_function');
    $mock->apply($call_with_input);

    $call_with_input->exec($input);
    $calls = $mock->call_args_array;

    ensure($calls[0][0] == $input);
}

