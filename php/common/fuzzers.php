<?php

namespace phpoc\fuzzer;

require_once "lib/php-sql-parser/vendor/autoload.php";

/**
 * Fuzzer implementation interface. Several methods are defined and documented
 * below. In addition to these methods, a fuzzer must implement several other
 * attributes, please the SQLIMySQLFuzzer implementation for details.
 *
 * $payloads contains an array with arrays which look as [payload, function_to_handle_results]
 *
 * That function will be called with the following parameters:
 *
 * Process calls to function after a single round of fuzzing.
 * @param string $round_id as above.
 * @param array $calls contains all calls received to all mocks after
 *  the function has been called with the malicious input given by fuzz.
 *
 * @param array $passed_args final args passed to the function under fuzz.
 */
interface PhpocFuzzer {

    /**
     * Returns a list of functions to mock.
     * @return array an array of strings which list all functions which need to
     * be mocked. Wildcards, like "mysql_*" are accepted, using http://php.net/fnmatch
     */
    public function mocks_required();

    /**
     * Returns a list of classes to mock.
     */
    public function class_mocks_required();

    /**
     * Returns a malicious string for a parameter.
     * @param string $round_id a unique string. Useful for correlating with
     * process.
     * @return: array('malicious_string', 'function_name');
     */
    public function fuzz($round_id); 


    /**
     * Returns a list of dangerous calls.
     * @return array Sink
     */
    public function dangerous_calls_get();

}

class SQLIMySQLFuzzer implements \phpoc\fuzzer\PhpocFuzzer {

    private $payloads = array(
        ["' or {{nb}} = {{nb}} -- ", "process_injection"],
        ["\" or {{nb}} = {{nb}} -- ", "process_injection"],
        ["{{nb}} or {{nb}} = {{nb}} -- ", "process_injection"],
        ["' or {{nb}} = {{nb}} #", "process_injection"],
        ["\" or {{nb}} = {{nb}} #", "process_injection"],
        ["{{nb}} or {{nb}} = {{nb}} #", "process_injection"]
    );

    private $counter = 0;
    private $last_round_id = "";
    private $last_payload = NULL;
    private $dangerous_calls;

    public function __construct() {
        $this->dangerous_calls = [];
    }

    public function mocks_required() {
        return ['mysql_*', 'mysqli_*'];
    }

    public function class_mocks_required() {
        return ["mysqli"];
    }

    public function fuzz($round_id) {
        return $this->payload_get($round_id);
    }

    public function process_injection($round_id, $calls, $passed_args) {
        $to_check = [
            'mysql_query' => 0,
            'mysqli_query' => 1,
            'query' => 0, # mysqli->query
        ];

        $injected = FALSE;
        foreach($calls as $call) {
            $key = $to_check[$call->function_name];

            if($key === NULL) {
                $key = @$to_check[$call->overriden_method];
            }

            if($key !== NULL) {
                $sql = $call->call_args[$key];
                $sql_error_eq_injected = !$call->is_global;
                
                $injected = $this->injection_detect($sql, $this->last_payload,
                    $sql_error_eq_injected);

                if($injected) {
                    break;
                }
            }
        }

        if($injected) {
            if(@!is_array($this->dangerous_calls[$round_id])) {
                $this->dangerous_calls[$round_id] = [];
            }

            $sink = new \phpoc\DangerousCall($call->function_name, $call->call_args, $sql);
            array_push($this->dangerous_calls[$round_id], $sink);
        }
    }

    /**
     * Simple heuristics to detect SQL injection in the WHERE clause.
     * @param string $sql the sql string received by the sql processing function.
     * @param array $last_payload last payload sent to the function.
     * @param bool $sql_error_eq_injected whether to treat invalid SQL syntax
     *  as a successful SQL injection or not.
     */
    private function injection_detect($sql, $last_payload, $sql_error_eq_injected = TRUE) {
        if($sql === $last_payload[0]) {
            return TRUE;
        }
        
        if(\phpoc\str_contains($sql, PAYLOAD_NB) || \phpoc\str_contains($sql, PAYLOAD_STR)) {
            $parser = new \PHPSQLParser\PHPSQLParser();
            $parsed = $parser->parse($sql);

            $sql_error = !$parsed;
            if($sql_error) {
                return $sql_error_eq_injected ? TRUE : FALSE;
            }

            foreach($parsed['WHERE'] as $token) {
                if($token['expr_type'] == "const") {
                    $val = $token["base_expr"];
                    if($val === PAYLOAD_NB || substr($val, 1, -1) === PAYLOAD_STR) {
                        return TRUE;
                    }
                }
            }
        }

        return FALSE;
    }

    /**
     * Performs some simple replacements on string.
     * @param string $string the variable to do the replacements on.
     */
    private function tpl($string) {
        $string = str_replace("{{nb}}", PAYLOAD_NB, $string);
        $string = str_replace("{{str}}", PAYLOAD_STR, $string);
        return $string;
    }

    private function payload_get($round_id) {
        if($this->last_round_id !== $round_id) {
            $this->counter = 0;
            $this->last_round_id = $round_id;
        } else {
            $this->counter++;
            if($this->counter > sizeof($this->payloads) - 1) {
                return NULL;
            }
        }

        $payload = $this->tpl($this->payloads[$this->counter]);
        $this->last_payload = $payload;
        return $payload;
    }

    public function dangerous_calls_get() {
        return $this->dangerous_calls;
    }
    
}
