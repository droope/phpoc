#include <unistd.h>
#include <fcntl.h>

#include "phpoc/analyse.h"
#include "phpoc/json.h"
#include "phpoc/list.h"
#include "phpoc/minunit.h"
#include "phpoc/rpc.h"
#include "phpoc/util.h"

#define TEST_TIMEOUT 10

char *target_file = "test_cases/sqli/recurse-test/another-folder/adodb-mysqli.inc.php";
char *target_dir = "test_cases/sqli/recurse-test/";

char *test_run_php_tests() {
    char *cmd[] = {"/usr/bin/php", "php/phpoc.php", "test", NULL};

    int outfp;
    pid_t pid = phpoc_popen(cmd, NULL, &outfp, NULL);
    fcntl(outfp, F_SETFL, O_NONBLOCK);

    sds out;
    int res = phpoc_pread_timeout(pid, outfp, 60, &out);
    printf("%s", out);
    mu_assert(res == 0, "PHP tests failed");

    sdsfree(out);

    return NULL;
}

char *test_opts_parse() {
    char *target_dir = "test_target/";
    char *argv[] = {"phpoc", target_dir};
    int argc = 2;

    Options *opts = opts_parse(argc, argv);
    mu_assert(strcmp(target_dir, opts->target) == 0, "Error parsing.");

    opts_free(opts);
    return NULL;
}

char *test_opts_parse_file() {
    char *target_file = "test_target";
    char *argv[] = {"phpoc", target_file};
    int argc = 2;

    Options *opts = opts_parse(argc, argv);
    mu_assert(strcmp(target_file, opts->target) == 0, "Error parsing.");

    opts_free(opts);
    return NULL;
}

char *test_append_slash() {
    char *with_slash = "phpoc/lib/test/";
    char *without_slash = "phpoc/lib/test";

    sds with_slash_app = phpoc_append_slash(with_slash);
    sds without_slash_app = phpoc_append_slash(without_slash);

    mu_assert(strcmp(with_slash, with_slash_app) == 0, "Unnecessary change");
    mu_assert(strcmp(with_slash, without_slash_app) == 0, "No slash appended");

    sdsfree(with_slash_app);
    sdsfree(without_slash_app);
        
    return NULL;
}

char *test_is_dir() {
    mu_assert(phpoc_is_dir(target_dir) == 1, "Directory detected as file");
    mu_assert(phpoc_is_dir(target_file) == 0, "File detected as dir.");
    return NULL;
}

char *test_parse_json() {
    sds json = NULL;
    phpoc_read_file("test_cases/misc/sample_file.json", &json);

    PhpocFile *file = NULL;
    json_parse_file(json, &file);

    PhpocSink *sink = file->sinks->first->value;
    DangerousCall *first_call = sink->calls->first->value;
    List *parameters = first_call->parent_parameters;

    mu_assert(file->sinks->count == 1, "Not the right nb of sinks.");
    mu_assert(sink->param_no == 0, "wrong param nb.");
    mu_assert(sink->calls->count == 2, "Wrong nb of calls");

    mu_assert_strcmp(file->path, "test_cases/sqli/simple.php");
    mu_assert_strcmp(sink->function_name, "dangerous");
    mu_assert_strcmp(first_call->function_name, "mysql_query");
    mu_assert_strcmp(parameters->first->value, "' or 490308101 = 490308101 -- ");
    mu_assert_strcmp(parameters->last->value, "placeholder_str");

    phpoc_file_free(file);
    sdsfree(json);
    return NULL;
}

char *test_phpoc_popen() {
    char *test_string = "test_string";
    char *cmd[] = {"echo", "-n", test_string, NULL};

    int outfp;
    phpoc_popen(cmd, NULL, &outfp, NULL);

    sds out = NULL;
    phpoc_read_timeout(outfp, TEST_TIMEOUT, &out);
    mu_assert(strcmp(test_string, out) == 0, "Incorrect stdout reading.");

    close(outfp);
    sdsfree(out);

    return NULL;
}

char *test_proc_stdout() {
    char *test_string = "test_string";
    char *cmd[] = {"echo", "-n", test_string, NULL};

    int outfp;
    pid_t pid = phpoc_popen(cmd, NULL, &outfp, NULL);
    fcntl(outfp, F_SETFL, O_NONBLOCK);

    sds out;
    phpoc_pread_timeout(pid, outfp, TEST_TIMEOUT, &out);
    mu_assert(strcmp(out, test_string) == 0, "Strings were not equal");
    sdsfree(out);

    return NULL;
}

char *test_proc_stdout_timeout() {
    char *cmd[] = {"sleep", "3", NULL};

    int outfp;
    pid_t pid = phpoc_popen(cmd, NULL, &outfp, NULL);
    fcntl(outfp, F_SETFL, O_NONBLOCK);

    sds out;
    int res = phpoc_pread_timeout(pid, outfp, 2, &out);
    sdsfree(out);

    mu_assert(res == -1, "Should have errored.");

    return NULL;
}

char *test_proc_stdout_error() {
    char *cmd[] = {"ls", "/non_existent_file_qdjwijqowdojqiwd", NULL};

    int outfp;
    pid_t pid = phpoc_popen(cmd, NULL, &outfp, NULL);

    sds out;
    int res = phpoc_pread_timeout(pid, outfp, TEST_TIMEOUT, &out);
    sdsfree(out);

    mu_assert(res > 0, "Should have errored.");

    return NULL;
}

char *test_phpoc_popen_stderr() {
    char *test_string = "test_string";
    sds cmd_str = sdsempty();
    cmd_str = sdscatfmt(cmd_str, "echo -n '%s' 1>&2", test_string);

    int errfp;
    char *cmd[] = {"/bin/sh", "-c", cmd_str, NULL};
    phpoc_popen(cmd, NULL, NULL, &errfp);

    sds err = NULL;
    phpoc_read_timeout(errfp, TEST_TIMEOUT, &err);
    mu_assert(strcmp(test_string, err) == 0, "Incorrect stdout reading.");

    sdsfree(err);
    sdsfree(cmd_str);
    close(errfp);

    return NULL;
}

char *test_phpoc_popen_stdin() {
    char *test_string = "test_string\n";
    char *cmd[] = {"cat", NULL};

    int outfp;
    int infp;
    pid_t pid = phpoc_popen(cmd, &infp, &outfp, NULL);
    fcntl(outfp, F_SETFL, O_NONBLOCK);

    write(infp, test_string, strlen(test_string));
    close(infp);

    sds out;
    phpoc_pread_timeout(pid, outfp, 10, &out);
    mu_assert(strcmp(out, test_string) == 0, "Strings were not equal");
    sdsfree(out);

    return NULL;
}

char *test_generate_rpc_json() {
    char *expected = "{ \"method\": \"function_name\", \"params\": [ \"arg1\", \"arg2\", \"arg3\" ] }";

    List *args = list_create();
    list_push(args, sdsnew("arg1"));
    list_push(args, sdsnew("arg2"));
    list_push(args, sdsnew("arg3"));

    sds out_json; 
    rpc_call_generate("function_name", args, &out_json);

    mu_assert_strcmp(expected, out_json);

    sdsfree(out_json);
    list_free_sds(args);

    return NULL;
}

char *test_simple_sqli_integration() {
    List *files = list_create();
    list_push(files, sdsnew("test_cases/sqli/simple.php"));

    List *file_sinks = NULL;
    identify_file_sinks(files, &file_sinks);

    PhpocFile *file = file_sinks->first->value;
    List *sinks = file->sinks;
    PhpocSink *sink = sinks->first->value;

    mu_assert(file_sinks->count == 1, "Should have one file");
    mu_assert(sinks->count == 1, "Should have one sink.");
    mu_assert(strcmp(sink->function_name, "dangerous") == 0, "Wrong sink name.");

    list_foreach(file_sinks, f) {
        phpoc_file_free(f->value);
    }
    list_destroy(file_sinks);
    list_free_sds(files);

    return NULL;
}

char *test_get_required_functions() {
    List *required_mocks = list_create();
    required_mocks_get(required_mocks);

    mu_assert(required_mocks->count > 0, "Not enough mocks.");

    list_free_sds(required_mocks);
    return NULL;
}

char *test_file_has_patterns() {
    List *required_mocks = list_create();
    required_mocks_get(required_mocks);

    char *file_pattern = "test_cases/sqli/simple.php";
    char *file_no_pattern = "test_cases/sqli/no_sql.php";

    mu_assert(required_mocks->count > 0, "Not enough mocks.");
    mu_assert(file_has_patterns(file_pattern, required_mocks) == 1, "Should have detected sql.");
    mu_assert(file_has_patterns(file_no_pattern, required_mocks) == 0, "Should have detected nothing.");

    list_free_sds(required_mocks);
    return NULL;
}

char *test_identify_files() {
    List *required_mocks = list_create();
    required_mocks_get(required_mocks);

    List *php_files = list_create();
    int res = identify_files(php_files, target_dir, required_mocks);

    mu_assert(res == 0, "Identify failed.");
    mu_assert(php_files->count == 1, "Unexpected file nb.");
    LIST_FOREACH(php_files, first, next, cur) {
        char *val = (char *) cur->value;
        mu_assert(strcmp(target_file, val) == 0, "Unknown file");
    }

    list_free_sds(php_files);
    list_free_sds(required_mocks);
    return NULL;
}

char *test_identify_files_false_pos() {
    char *target_dir = "test_cases/sqli/";
    List *required_mocks = list_create();
    required_mocks_get(required_mocks);

    List *php_files = list_create();
    int res = identify_files(php_files, target_dir, required_mocks);

    mu_assert(res == 0, "Identify failed.");
    mu_assert(php_files->count > 0, "Unexpected file nb.");
    int contains_fp = 0;
    list_foreach(php_files, file) {
        sds filename = file->value;
        debug("%s", filename);
        if(strcmp("test_cases/sqli/no_sql.php", filename) == 0) {
            contains_fp = 1;
        }
    }

    mu_assert(contains_fp == 0, "Found non useful file");

    list_free_sds(php_files);
    list_free_sds(required_mocks);
    return NULL;
}
char *test_overrides_get() {
    sds json = NULL;
    phpoc_read_file("test_cases/misc/sample_file.json", &json);

    List *file_sinks = list_create();
    PhpocFile *file = NULL;
    json_parse_file(json, &file);
    list_push(file_sinks, file);

    sds *out_json;
    rpc_overrides_generate(file_sinks, &out_json);

    printf("%s\n", out_json);

    list_destroy(file_sinks);
    phpoc_file_free(file);
    sdsfree(json);
    sdsfree(out_json);

    return NULL;
}

char *all_tests() {
    mu_suite_start();

    /*mu_run_test(test_run_php_tests);*/
    /*mu_run_test(test_opts_parse);*/
    /*mu_run_test(test_opts_parse_file);*/
    /*mu_run_test(test_append_slash);*/
    /*mu_run_test(test_is_dir);*/
    /*mu_run_test(test_parse_json);*/
    /*mu_run_test(test_phpoc_popen);*/
    /*mu_run_test(test_proc_stdout);*/
    /*mu_run_test(test_proc_stdout_timeout);*/
    /*mu_run_test(test_proc_stdout_error);*/
    /*mu_run_test(test_phpoc_popen_stderr);*/
    /*mu_run_test(test_phpoc_popen_stdin);*/
    /*mu_run_test(test_generate_rpc_json);*/
    /*mu_run_test(test_simple_sqli_integration);*/
    /*mu_run_test(test_get_required_functions);*/
    /*mu_run_test(test_file_has_patterns);*/
    /*mu_run_test(test_identify_files);*/
    /*mu_run_test(test_identify_files_false_pos);*/
    mu_run_test(test_overrides_get);

    return NULL;
}

RUN_TESTS(all_tests);
