<?php

namespace phpoc;

require_once "common/include.php";

/**
 * Calls RPC method via STDIN or via a command line argument.
 * A JsonRPCResponse is serialized into stdout as a response.
 */
function rpc_init($method) {
    if(!$method) {
        $stdin_text = file_get_contents("php://stdin");
        $parsed = json_decode($stdin_text);

        $method = $parsed->method;
        $params = $parsed->params;
    } else {
        $params = func_get_args();
        array_shift($params);
    }

    $method_final = "\\phpoc\\rpc_".$method;
    $resp = call_user_func_array($method_final, $params);

    echo json_encode($resp);
}

function rpc_required_mocks() {
    $fuzzers = \phpoc\fuzzer\fuzzers_get_all();

    $results = [];
    foreach($fuzzers as $fuzzer_class) {
        $fuzzer = new $fuzzer_class;
        foreach($fuzzer->mocks_required() as $mock_str) {
            array_push($results, str_replace("*", "", $mock_str));
        }

        foreach($fuzzer->class_mocks_required() as $mock_str) {
            array_push($results, str_replace("*", "", $mock_str));
        }
    }

    $resp = new JsonRPCResponse($results);

    return $resp;
}

function rpc_pwn($target) {
    if(!GlobalOptions::get('debug')) {
        ob_start();
    }

    $sinks = fuzzer\fuzz($target);
    $file = new PhpocFile($target, $sinks);

    if(!GlobalOptions::get('debug')) {
        ob_end_clean();
    }

    return $file;
}
