#pragma once

int json_parse_file(sds json_str, PhpocFile **file);
int json_parse_rpc_list(sds rpc_response, List *l);

#define json_obj_foreach(json_obj) check(json_obj->type == json_object, "Expected an object."); \
    int json_obj_i; \
    json_value *json_obj_internal = json_obj; \
    for(json_obj_i = 0; json_obj_i < json_obj->u.object.length; json_obj_i++) 

#define json_obj_get(key, val) char *key = json_obj_internal->u.object.values[json_obj_i].name; \
        json_value *val = json_obj_internal->u.object.values[json_obj_i].value;

#define json_arr_foreach(json_arr) check(json_arr->type == json_array, "Expected array at root."); \
    int json_arr_i; \
    json_value *json_arr_internal = json_arr; \
    for(json_arr_i = 0; json_arr_i < json_arr->u.array.length; json_arr_i++)

#define json_arr_get(val) json_value *val = json_arr_internal->u.array.values[json_arr_i]
