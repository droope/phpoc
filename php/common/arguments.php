<?php

namespace phpoc;

$help_tpl = <<<'EOD'

Usage: php phpoc.php [command] [arguments] [options]

Commands:
{{commands}}

Global options:
{{options}}

Example:
php phppoc.php pwn tests/sqli/medium.php

EOD;

$command_tpl = <<<'EOD'
    {{command_name}} {{args}}
EOD;

define('HELP_TPL', $help_tpl);
define('COMMAND_TPL', $command_tpl);

class GlobalOptions {
    static $options = [];

    static function set($name, $value) {
        self::$options[$name] = $value;
    }

    static function get($name) {
        return @self::$options[$name];
    }

    static function opts_set($options) {
        foreach($options as $opt) {
            if($opt['action'] == 'store_true') {
                self::set($opt['target'], TRUE);
            }
        }
    }
}

class Arguments {

    public $controller = NULL;
    public $params = NULL;
    public $valid_opts = [
        'd|debug' => [
            'target' => 'debug',
            'action' => 'store_true',
            'help' => 'Enables debugging.'
        ]
    ];

    function __construct($argv) {
        if(sizeof($argv) == 1) {
            $this->display_help();
            exit(1);
        } 

        list($arguments, $options) = $this->parse(array_slice($argv, 1));

        $this->controller = $arguments[0];
        $this->params = array_slice($arguments, 1);
        GlobalOptions::opts_set($options);
    }

    function parse($argv) {
        $arguments = [];
        $options = [];
        $i = 0;
        foreach($argv as $arg) {
            if(starts_with($arg, '--')) {
                $this->parse_opt($options, $argv, $i, 'long');
            } else if(starts_with($arg, '-')) {
                $this->parse_opt($options, $argv, $i, 'short');
            } else {
                array_push($arguments, $arg);
            }
            $i++;
        }

        return [$arguments, $options];
    }

    function parse_opt(&$options, $argv, $i, $which) {
        $arg = $argv[$i];
        $skip = $which === 'short' ? 1 : 2;

        $valid_opts = $this->opts_get($which); 
        $opt = substr($arg, $skip);
        if(!in_array($opt, $valid_opts)) {
            throw new UserMessageException("Invalid opt $opt.\n");
        }

        array_push($options, $this->opt_get($opt, $which));
    }

    function opt_get($opt, $which) {
        $index = $which === 'short' ? 0 : 1;
        foreach($this->valid_opts as $k => $valid_opt) {
            $exp = explode("|", $k);
            if($exp[$index] === $opt) {
                return $valid_opt;
            }
        }

        return NULL;
    }

    function opts_get($which) {
        $index = $which === 'short' ? 0 : 1;

        $opts = [];
        foreach($this->valid_opts as $o => $vals) {
            $exp = explode("|", $o);
            array_push($opts, $exp[$index]);
        }
        
        return $opts;
    }

    function display_help() {
        $controllers = $this->get_controllers();
        $commands = $this->get_commands($controllers);
        $options = $this->get_options($this->valid_opts);
        $tpl = new Template(HELP_TPL, ['commands' => $commands,
            'options' => $options]);
        echo $tpl->render();
    }

    function get_options($valid_opts) {
        $lines = [];
        foreach($valid_opts as $k => $opt) {
            list($short, $long) = explode("|", $k);
            $str = "    -$short --$long {$opt['help']}";
            array_push($lines, $str);
        }

        return implode("\n", $lines);
    }

    function get_args($command) {
        $args = [];
        foreach($command->get_params() as $param) {
            array_push($args, "[{$param->name}]");
        }

        return implode(" ", $args);
    }

    function get_commands($controllers) {
        $commands = [];
        foreach($controllers as $controller) {
            $vars = [
                'command_name' => substr($controller->name, 0, -5),
                'args' => $this->get_args($controller)
            ];

            $tpl = new Template(COMMAND_TPL, $vars);
            array_push($commands, $tpl->render());
        }

        return implode("\n", $commands);
    }

    function get_controllers() {
        $controllers = [];
        $files = scandir(CONTROLLERS_DIR);
        foreach($files as $file) {
            if(ends_with($file, '.php')) {
                $filename_full = CONTROLLERS_DIR . $file;
                $controller_func = basename($file, '.php') . "_init";
                
                $function = \phpoc\parser\function_get($filename_full,
                    $controller_func);
                array_push($controllers, $function);
            }
        }

        return $controllers;
    }

}

class Template {
    private $template;
    private $vars;

    public function __construct($template, $vars) {
        $this->template = $template;
        $this->vars = $vars;
    }

    public function render() {
        $callback = [$this, "replace_var"];
        $str = preg_replace_callback("#\{\{[a-zA-Z0-9_]*?\}\}#", $callback, $this->template);
        return $str;
    }

    public function replace_var($matches) {
        $var = substr($matches[0], 2, -2);
        return $this->vars[$var];
    }
}
