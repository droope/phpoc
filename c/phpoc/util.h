#pragma once 

#include "lib/sds/sds.h"
#include "phpoc/list.h"

struct Options;
typedef struct Options {
    sds target;
} Options;

void display_help();
struct Options* opts_parse(int argc, char *argv[]);
void opts_free(struct Options *opts); 

char *phpoc_append_slash(char *str);
int phpoc_endswith(char *haystack, char *needle);
int phpoc_is_dir(char *path);
int phpoc_read_file(const char *path, sds *out);

pid_t phpoc_popen(char **command, int *infp, int *outfp, int *errfp);
int phpoc_pread_timeout(pid_t pid, int outfp, int timeout, sds *out);
int phpoc_read_timeout(int fp, int timeout, sds *out);
