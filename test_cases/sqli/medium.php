<?php

function bad_include($id) {
    include "werp/badinclude.php";
    $sql = "SELECT first_name FROM users WHERE user_id = '$id';";
    $result = mysql_query ($sql);
    return NULL;
}

function bad_require($id) {
    require "werp/badrequire.php";
    $sql = "SELECT first_name FROM users WHERE user_id = '$id';";
    $result = mysql_query ($sql);
    return NULL;
}

function bad_include_once($id) {
    include_once "werp/badinclude_once.php";
    $sql = "SELECT first_name FROM users WHERE user_id = '$id';";
    $result = mysql_query ($sql);
    return NULL;
}

function bad_require_once($id) {
    require_once "werp/badinclude.php";
    $sql = "SELECT first_name FROM users WHERE user_id = '$id';";
    $result = mysql_query ($sql);
    return NULL;
}

function missing_function($id) {
    $var = enable_supercool_hat($id);

    $sql = "SELECT first_name FROM users WHERE user_id = '$id';";
    $result = mysql_query ($sql);

    return $var;
}

function missing_function_space($id) {
    $var = enable_supercool_hat ($id);

    $sql = "SELECT first_name FROM users WHERE user_id = '$id';";
    $result = mysql_query ($sql);

    return $var;
}

function if_else_simple($id) {
    if(TRUE) {
        $sql = "SELECT first_name FROM users WHERE user_id = '$id';";
        $result = mysql_query ($sql);
    } else {
        // pass
    }
}

function pass_by_reference($id, &$whatever) {
    $sql = "SELECT first_name FROM users WHERE user_id = '$id';";
    $result = mysql_query ($sql);
}

class BasicClassTest {

    $property = "heh";

    function basic_class_property($id) {
        $this->property = $id;
        $sql = "SELECT first_name FROM users WHERE user_id = '$id';";
        $result = mysql_query ($sql);
    }

    function basic_class_func($id) {
        $this->test_this($id);
        $sql = "SELECT first_name FROM users WHERE user_id = '$id';";
        $result = mysql_query ($sql);
    }

    function test_this() {
    }
}

function mysql_connect_timeout($id) {
    mysql_connect("wdqwdqw");
    $sql = "SELECT first_name FROM users WHERE user_id = '$id';";
    $result = mysql_query ($sql);
}

function mysql_query_raw($sql) {
    $result = mysql_query ($sql);
    return NULL;
}

function mysqli_procedural($id) {
    $mysqli = mysqli_connect("example.com", "user", "password", "database");
    $res = mysqli_query($mysqli, "SELECT * FROM USERS WHERE userid = '$id'");
    $row = mysqli_fetch_assoc($res);
    echo $row['_msg'];
}

function mysqli_procedural_safe($id) {
    $id = (int) $id;
    $mysqli = mysqli_connect("example.com", "user", "password", "database");
    $res = mysqli_query($mysqli, "SELECT * FROM USERS WHERE userid = '$id'");
    $row = mysqli_fetch_assoc($res);
    echo $row['_msg'];
}

function mysqli_oo($id) {
    $mysqli = new mysqli("example.com", "user", "password", "database");
    if ($mysqli->connect_errno) {
        echo "Failed to connect to MySQL: " . $mysqli->connect_error;
    }

    $res = $mysqli->query("SELECT * FROM USERS WHERE userid = '$id'");
    $row = $res->fetch_assoc();
    echo $row['_msg'];
}

function mysqli_oo_fuzzy($id) {
    global $mysqli, $nothing;

    $res = $mysqli->query("SELECT * FROM USERS WHERE userid = '$id'");
    $row = $res->fetch_assoc();
}

function mysqli_oo_fuzzy_false_pos($orange_cat_name) {
    global $mysqli, $nothing;

    $res = $mysqli->query("$orange_cat_name IS AN ORANGE CAT LOL!");
    $row = $res->fetch_assoc();
}

function mysqli_oo_safe($id) {
    $id = (int) $id;
    $mysqli = new mysqli("example.com", "user", "password", "database");
    $res = $mysqli->query("SELECT * FROM USERS WHERE userid = '$id'");
    $row = $res->fetch_assoc();
}

function missing_class($id) {
    $var = new VariableCompilerOrangeSuperHat();
    $var->wear();

    Globals::set_hats($var);
    
    $mysqli = mysqli_connect("example.com", "user", "password", "database");
    $res = mysqli_query($mysqli, "SELECT * FROM USERS WHERE userid = '$id'");
    $row = mysqli_fetch_assoc($res);
    echo $row['_msg'];
}

function complex_string($id)
{
    // crash caused by complex string syntax:
    // https://secure.php.net/manual/en/language.types.string.php#language.types.string.parsing.complex
    if ($this->databaseType == 'oci8po') 
            $sql = "UPDATE $table set $column=EMPTY_{$blobtype}() WHERE $where RETURNING $column INTO ?";
    else 
            $sql = "UPDATE $table set $column=EMPTY_{$blobtype}() WHERE $where RETURNING $column INTO :blob";
    
    $mysqli = mysqli_connect("example.com", "user", "password", "database");
    $res = mysqli_query($mysqli, "SELECT * FROM USERS WHERE userid = '$id'");
    $row = mysqli_fetch_assoc($res);
    echo $row['_msg'];
}

function exit_die($id) {
    exit;
    die;
    exit();
    die();
    exit("lel");
    die("lel");
    exit(1);
    die(1);

    $mysqli = mysqli_connect("example.com", "user", "password", "database");
    $res = mysqli_query($mysqli, "SELECT * FROM USERS WHERE userid = '$id'");
    $row = mysqli_fetch_assoc($res);
    echo $row['_msg'];
}

function  sleep_usleep($id) {
    sleep(10);
    usleep(100000);
    time_sleep_until(123123123123);
    time_nanosleep(1231231, 123123123);

    $mysqli = mysqli_connect("example.com", "user", "password", "database");
    $res = mysqli_query($mysqli, "SELECT * FROM USERS WHERE userid = '$id'");
    $row = mysqli_fetch_assoc($res);
    echo $row['_msg'];
}

function mysqli_oo_fuzzy_other_var_name($id) {
    global $whatever, $nothing;

    $res = $whatever->query("SELECT * FROM USERS WHERE userid = '$id'");
    $row = $res->fetch_assoc();
}

function override_simple($id) {
    $sql = "SELECT first_name FROM users WHERE user_id = '$id';";
    $result = foo_bar($sql);

    return NULL;
}
