#include <dirent.h>
#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>

#include "phpoc/analyse.h"
#include "phpoc/dbg.h"
#include "phpoc/json.h"
#include "phpoc/rpc.h"
#include "phpoc/util.h"

#define MAX_THREADS 10
#define PROCESS_TIMEOUT 60
#define DEBUG 0

void phpoc_file_free(PhpocFile *file) {
    if(file == NULL) {
        return;
    }

    sdsfree(file->path);
    list_foreach(file->sinks, sink_ln) {
        PhpocSink *sink = sink_ln->value;

        sdsfree(sink->function_name);
        list_foreach(sink->calls, call_ln) {
            DangerousCall *call = call_ln->value;

            sdsfree(call->function_name);
            sdsfree(call->sink_called_with);
            list_free_sds(call->parent_parameters);
        }

        if(sink->calls != NULL) {
            list_free(sink->calls);
        }
        free(sink);
    }

    if(file->sinks != NULL) {
        list_destroy(file->sinks);
    }

    free(file);
}

/**
 * Runs phpoc on a target_file and populates a list of sinks.
 */
void phpoc_results(sds target_file, PhpocFile **file) {
    List *args = list_create();
    list_push(args, target_file);

    sds rpc_response;
    rpc_call("pwn", args, PROCESS_TIMEOUT, &rpc_response);

    json_parse_file(rpc_response, file);

    sdsfree(rpc_response);
    list_destroy(args);
}

/**
 * Threads' main entry point. Loops forever until it runs out of tasks.
 */
void *identify_sinks_thread(void *args_void) {
    ist_args *args = args_void;

    while(1) {
        pthread_mutex_lock(&args->tasks_lock);
        debug("Tasks left: %d", args->tasks->count);
        sds target_file = list_shift(args->tasks);
        pthread_mutex_unlock(&args->tasks_lock);

        if(target_file == NULL) {
            break;
        }

        PhpocFile *file = NULL;
        phpoc_results(target_file, &file);

        if(file != NULL) {
            pthread_mutex_lock(&args->sinks_lock);
            list_push(args->file_sinks, file);
            pthread_mutex_unlock(&args->sinks_lock);
        }

        sdsfree(target_file);
    }

    return NULL;
}

/**
 * Identifies vulnerabilities in PHP files.
 */
int identify_file_sinks(const List *php_files, List **file_sinks) {
    List *tasks = list_create();
    LIST_FOREACH(php_files, first, next, cur) {
        list_push(tasks, sdsnew((char *) cur->value));
    }

    ist_args *args = calloc(1, sizeof(ist_args));
    args->tasks = tasks;

    *file_sinks = list_create();
    args->file_sinks = *file_sinks;

    check(pthread_mutex_init(&args->tasks_lock, NULL) == 0, "Can't initialize tasks mutex.");
    check(pthread_mutex_init(&args->sinks_lock, NULL) == 0, "Can't initialize sinks mutex.");

    int i;
    pthread_t threads[MAX_THREADS];
    for(i = 0; i < MAX_THREADS; i++) {
        pthread_create(&threads[i], NULL, identify_sinks_thread, args);
    }

    for(i = 0; i < MAX_THREADS; i++) {
        pthread_join(threads[i], NULL);
    }

    free(args);
    list_free(tasks);

    return 0;
error:
    return -1;
}

/**
 * Populates php_files with a list of php files in target_dir.
 * @param a list in order to populate
 * @param the target directory.
 */
int identify_files(List *php_files, char *target_dir, List *required_mocks) {
    DIR *dp;
    dp = opendir(target_dir);
    check(dp != NULL, "Could not open directory %s", target_dir);

    sds full_path;
    struct dirent *file;
    while((file = readdir(dp)) != NULL) {
        full_path = phpoc_append_slash(target_dir);
        full_path = sdscat(full_path, file->d_name);
        if(file->d_type == DT_DIR) {
            if(!strcmp(file->d_name, ".") == 0 && !strcmp(file->d_name, "..") == 0) {
                identify_files(php_files, full_path, required_mocks);
            }
            sdsfree(full_path);
        } else {
            if(phpoc_endswith(full_path, ".php") &&
                    file_has_patterns(full_path, required_mocks)) { 

                list_push(php_files, full_path);
            } else {
                sdsfree(full_path);
            }
        }
    }

    closedir(dp);
    return 0;
error:
    return -1;
}

int identify_sinks(char *target, List **file_sinks, sds func_override) {
    int ret_code = 0, is_dir = -1;
    List *php_files = list_create();

    is_dir = phpoc_is_dir(target);
    if(is_dir == 1) {
        List *required_mocks = list_create();
        required_mocks_get(required_mocks);

        int if_ret = identify_files(php_files, target, required_mocks);
        list_free_sds(required_mocks);

        if(if_ret == -1) {
            ret_code = -1;
            goto cleanup;
        }

    } else if(is_dir == 0) {
        list_push(php_files, sdsnew(target));
    } else {
        goto cleanup;
    }

    if(identify_file_sinks(php_files, file_sinks) == -1) {
        ret_code = -1;
        goto cleanup;
    }

cleanup:
    list_free_sds(php_files);
    return ret_code;
}

/**
 * Populates a list of required mocks into the required mocks array.
 */
void required_mocks_get(List *required_mocks) {
    sds rpc_response;
    rpc_call("required_mocks", NULL, PROCESS_TIMEOUT, &rpc_response);

    json_parse_rpc_list(rpc_response, required_mocks);

    sdsfree(rpc_response);
}

/**
 * Returns 1 when file contains a pattern in patterns. Returns 0 otherwise.
 * Returns -1 on error.
 */
int file_has_patterns(char *filename, List *patterns) {
    FILE *fp = NULL;
    char *line = NULL;
    size_t n = 0;
    int result = 0;

    fp = fopen(filename, "r");
    check(fp != NULL, "Could not open file");

    ssize_t r = 0;
    while((r = getline(&line, &n, fp)) != -1) {
        list_foreach(patterns, p) {
            sds pattern = p->value;
            if(strstr(line, pattern) != NULL) {
                result = 1;
            }
        }

        if(result == 1) {
            break;
        }
    }

    free(line);
    fclose(fp);

    return result;
error:
    return -1;
}
