<?php

namespace phpoc;

require_once "common/arguments.php";
require_once "common/classes.php";
require_once "common/fuzz.php";
require_once "common/constants.php";
require_once "common/fuzzers.php";
require_once "common/parser.php";

class UserMessageException extends \Exception {
}

function assert_alnum($str) {
    if(!ctype_alnum($str)) {
        throw new \Exception("Invalid characters in string.");
    }
}

function usage() {
    return "Usage: php phpoc [subcommand]\n";
}

function starts_with($haystack, $needle)
{
    $length = strlen($needle);
    if ($length == 0) {
        throw new Exception("Empty needle.");
    }

    return (substr($haystack, 0, $length) === $needle);
}

function ends_with($haystack, $needle)
{
    $length = strlen($needle);
    if ($length == 0) {
        throw new Exception("Empty needle.");
    }

    return (substr($haystack, -$length) === $needle);
}

function random_str($length = 10) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}

/**
 * Standard function to get a unique identifier for a mock.
 */
function uid_get() {
    return random_str(10);
}

function str_contains($str, $str_contains) {
    return strpos($str, $str_contains) !== FALSE;
}

function debug($str) {
    if(GlobalOptions::get('debug')) {
        echo "[DEBUG] $str \n";
    }
}

