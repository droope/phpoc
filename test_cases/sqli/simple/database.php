<?php
function db_connect_real($host, $user, $pass, $db_name, $db_type, $port = "3306", $db_ssl = false, $retries = 20) {
	global $cnn_id;

	$i = 0;
	$dsn = "$db_type://" . rawurlencode($user) . ":" . rawurlencode($pass) . "@" . rawurlencode($host) . "/" . rawurlencode($db_name) . "?persist";

	if ($db_ssl && $db_type == "mysql") {
		$dsn .= "&clientflags=" . MYSQL_CLIENT_SSL;
	}elseif ($db_ssl && $db_type == "mysqli") {
		$dsn .= "&clientflags=" . MYSQLI_CLIENT_SSL;
	}

	if ($port != "3306") {
		$dsn .= "&port=" . $port;
	}

	while ($i <= $retries) {
		$cnn_id = ADONewConnection($dsn);
		if ($cnn_id) {
			return($cnn_id);
		}

		$i++;

		usleep(40000);
	}

	die("FATAL: Cannot connect to MySQL server on '$host'. Please make sure you have specified a valid MySQL database name in 'include/config.php'\n");

	return(0);
}

function db_fetch_row($sql, $log = TRUE, $db_conn = FALSE) {
	global $cnn_id;

	/* check for a connection being passed, if not use legacy behavior */
	if (!$db_conn) {
		$db_conn = $cnn_id;
	}

	$sql = str_replace("\n", "", str_replace("\r", "", str_replace("\t", " ", $sql)));

	if (($log) && (read_config_option("log_verbosity") == POLLER_VERBOSITY_DEVDBG)) {
		cacti_log("DEVEL: SQL Row: \"" . $sql . "\"", FALSE);
	}

	$db_conn->SetFetchMode(ADODB_FETCH_ASSOC);
	$query = $db_conn->Execute($sql);

	if (($db_conn->ErrorNo() == 0) || ($db_conn->ErrorNo() == 1032)) {
		if (!$query->EOF) {
			$fields = $query->fields;

			$query->close();

			return($fields);
		}
	}else if (($db_conn->ErrorNo() == 1049) || ($db_conn->ErrorNo() == 1051)) {
		printf("FATAL: Database or Table does not exist");
		exit;
	}else if (($log) || (read_config_option("log_verbosity") >= POLLER_VERBOSITY_DEBUG)) {
		cacti_log("ERROR: SQL Row Failed!, Error:'" . $db_conn->ErrorNo() . "', SQL:\"" . str_replace("\n", "", str_replace("\r", "", str_replace("\t", " ", $sql))) . "\"", FALSE);
	}
}
