<?php

namespace phpoc;
require_once "common/include.php";
require_once "tests/assertions.php";

function test_init($single_test = NULL) {
    require_test_files();
    $tests = get_tests($single_test);
    $num_failed = run_tests($tests);

    if($num_failed > 0) {
        exit(1);
    }
}

function require_test_files() {
    if($handle = opendir(TESTS_DIR)) {
        while(false !== ($file = readdir($handle))) {
            if(0 === strpos($file, "test_")) {
                require TESTS_DIR . $file;
            }
        }
    }
}

function get_tests($single_test) {
    $all_functions = get_defined_functions()['user'];
    if($single_test) {
        $test_functions = array_filter($all_functions, function ($elem) use ($single_test) {
            return substr($elem, -1 * strlen($single_test)) == $single_test;
        });
    } else {
        $test_functions = array_filter($all_functions, function($elem) {
            return 0 === strpos($elem, "phpoc\\test\\test_");
        });
    }

    return $test_functions;
}

function run_tests($tests) {
    $failed = array();
    foreach($tests as $test) {
        try {
            echo $test . " ";
            call_user_func($test);
            echo "OK\n";
        } catch(\Exception $e) {
            array_push($failed, array($test, $e));
            echo "Failed\n";
        }
    }

    $total = sizeof($tests);
    $num_failed = sizeof($failed);

    echo "\n";

    if($num_failed > 0) {
        foreach($failed as $fail) {
            $test = $fail[0];
            $exc = $fail[1];
            $trace = $exc->getTraceAsString();
            $exc_type = get_class($exc);
            $exc_msg = $exc->getMessage();
            echo "${test} failed: Uncaught exception of type ${exc_type} with message '${exc_msg}'\n ${trace}\n---";
        }

        echo "\n\n";
    }


    echo "Tests: $total total, $num_failed failed.\n";

    return $num_failed;
}
