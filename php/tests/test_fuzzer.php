<?php

namespace phpoc\test;
use \phpoc\parser as parser;
use \phpoc\fuzzer as fuzzer;

require_once "common/include.php";

function assert_sinks_basic($sinks) {
    ensure(sizeof($sinks) === 1);
    ensure($sinks[0]->param_no === 0);
}

function simple_sqli_test($test_file, $function_name, $override = NULL) {
    $function = parser\function_get(TESTS_SQLI_DIR . "$test_file.php", $function_name);
    $sinks = fuzzer\function_fuzz($function, $override);

    assert_sinks_basic($sinks);
}

function test_fuzzer_dangerous() {
    $function = parser\function_get(TESTS_SQLI_DIR . "simple.php", "dangerous");
    $sinks = fuzzer\function_fuzz($function);

    ensure(sizeof($sinks) === 1);
    ensure($sinks[0]->function_name === "dangerous");
    ensure($sinks[0]->param_no === 0);
}

function test_fuzz_mysql_file() {
    $fuzz_file = TESTS_SQLI_DIR . "simple.php";
    $sinks = fuzzer\fuzz($fuzz_file);

    ensure(sizeof($sinks) === 1);
    ensure($sinks[0]->function_name === "dangerous");
    ensure($sinks[0]->param_no === 0);
}

function test_fuzzer_bad_include() {
    simple_sqli_test("medium", "bad_include");
    simple_sqli_test("medium", "bad_include_once");
}

function test_fuzzer_bad_require() {
    simple_sqli_test("medium", "bad_require");
    simple_sqli_test("medium", "bad_require_once");
}

function test_fuzzer_missing_function() {
    simple_sqli_test("medium", "missing_function");
}

function test_fuzzer_missing_function_space() {
    simple_sqli_test("medium", "missing_function_space");
}

function test_fuzzer_if_else_simple() {
    simple_sqli_test("medium", "if_else_simple");
}

function test_fuzzer_pass_by_reference() {
    simple_sqli_test("medium", "pass_by_reference");
}

function test_fuzzer_complex() {
    $function = parser\function_get(TESTS_SQLI_DIR . "CVE-2015-8369.php", "rrdtool_function_xport");
    $sinks = fuzzer\function_fuzz($function);

    ensure(sizeof($sinks) === 1);
    ensure($sinks[0]->param_no === 1);
}

function test_basic_class_property() {
    simple_sqli_test("medium", "basic_class_property");
}

function test_basic_class_func() {
    simple_sqli_test("medium", "basic_class_func");
}

function test_mysql_connect_timeout() {
    simple_sqli_test("medium", "mysql_connect_timeout");
}

function test_mysql_raw_sql() {
    simple_sqli_test("medium", "mysql_query_raw");
}

function test_mysqli_procedural() {
    simple_sqli_test("medium", "mysqli_procedural");
}

function test_mysqli_procedural_safe() {
    $function = parser\function_get(TESTS_SQLI_DIR . "medium.php", "mysqli_procedural_safe");
    $sinks = fuzzer\function_fuzz($function);

    ensure(sizeof($sinks) === 0);
}

function test_mysqli_oo() {
    simple_sqli_test("medium", "mysqli_oo");
}

function test_mysqli_fuzzy() {
    simple_sqli_test("medium", "mysqli_oo_fuzzy");
}

function test_mysqli_false_positive() {
    $function = parser\function_get(TESTS_SQLI_DIR . "medium.php", "mysqli_oo_fuzzy_false_pos");
    $sinks = fuzzer\function_fuzz($function);

    ensure(sizeof($sinks) === 0);
}

function test_mysqli_oo_safe() {
    $function = parser\function_get(TESTS_SQLI_DIR . "medium.php", "mysqli_oo_safe");
    $sinks = fuzzer\function_fuzz($function);

    ensure(sizeof($sinks) === 0);
}

function test_missing_class() {
    simple_sqli_test("medium", "missing_class");
}

function test_fuzzer_recurse() {
    ob_start();
    $files = fuzzer\fuzz_dir(TESTS_SQLI_DIR . "recurse-test");     
    ob_end_clean();
    $sinks = $files[0]->sinks;

    ensure(sizeof($files) === 1);
    ensure(sizeof($sinks) === 1);
    ensure($sinks[0]->param_no === 0);
}

function test_cropped_func_bug() {
    simple_sqli_test("medium", "complex_string");
}

function test_die_exit() {
    simple_sqli_test("medium", "exit_die");
}

function test_sleep() {
    simple_sqli_test("medium", "sleep_usleep");
}

function test_mysqli_fuzzy_other_var_name() {
    simple_sqli_test("medium", "mysqli_oo_fuzzy_other_var_name");
}

function test_override_simple() {
    $override = [
        'foo_bar' => 'mysql_query'
    ];

    simple_sqli_test("medium", "override_simple", $override);    
}
