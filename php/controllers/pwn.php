<?php

namespace phpoc;

require_once "common/include.php";

function pwn_init($target, $function = NULL) {
    if(is_dir($target)) {
        $files = fuzzer\fuzz_dir($target);
    } else {
        $sinks = fuzzer\fuzz($target, $function);
        $file = new PhpocFile($target, $sinks);
        $files = [$file];
    }

    echo json_encode($files) . "\n";
}
