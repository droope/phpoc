<?php

namespace phpoc\parser;

class ParserError extends \Exception {
}

/**
 * Missing functions are replaced with this.
 */
function function_missing() {
}

function functions_get($file) {
    $file_contents = file_get_contents($file);
    $tokens = token_get_all($file_contents);

    $current_function = NULL;
    $functions = array();
    $curly_bracket_depth = 0;
    $bracket_depth = 0;
    foreach($tokens as $token) {
        if($token[0] == T_FUNCTION) {
            $current_function = new \phpoc\PhpocFunction();
        }

        if($current_function) {
            $is_closing_curly = $token === "}";
            $is_closing_bracket = $token === ")";
            $is_function_body = $curly_bracket_depth && !$is_closing_curly;
            $is_opening_curly = $token === "{" || $token[0] === T_CURLY_OPEN;

            array_push($current_function->tokens, $token);
            if($is_function_body) {
                array_push($current_function->body_tokens, $token);
            } else {
                if($bracket_depth && !$is_closing_bracket) {
                    array_push($current_function->param_tokens, $token);
                }

                if($token === "(") {
                    $bracket_depth++;
                } else if($is_closing_bracket) {
                    $bracket_depth--;
                }
            }

            if($is_opening_curly) { 
                $curly_bracket_depth++;
            }

            if($token[0] == T_STRING && !$current_function->name) {
                $current_function->name = $token[1];
            } elseif(is_string($token)) {
                if($is_closing_curly) {
                    $curly_bracket_depth--;

                    $is_last_curly = $current_function && $curly_bracket_depth == 0;
                    if($is_last_curly) {
                        array_push($functions, $current_function);
                        $current_function = NULL;
                    } else {
                        array_push($current_function->body_tokens, $token);
                    }
                }
            }
        }
    }

    return $functions;
}

function function_get($file, $function_name) {
    $functions = functions_get($file);
    foreach($functions as $function) {
        if($function->name === $function_name) {
            return $function;
        }
    }

    return NULL;
}

/**
 * Replaces function calls to $orig_func_name to $new_func_name in a set of
 * $tokens.
 * @param array $tokens the tokens we'll work with.
 * @param string $orig_func_name the original function name, e.g. mysql_query.
 * @param string $new_func_name the new function name, e.g mock_mysql_query.
 */
function replace_function($tokens, $orig_func_name, $new_func_name) {
    $i = 0;
    $new_tokens = [];
    foreach($tokens as $token) {
        $is_function = is_standalone_function_call($tokens, $i);
        if ($is_function && $token[1] === $orig_func_name) {
            $new_token = array(T_STRING, $new_func_name, $token[2]);
            array_push($new_tokens, $new_token);
        } else {
            array_push($new_tokens, $token);
        }
        $i++;
    }

    return $new_tokens;
}

/**
 * As above, but for classes.
 */
function replace_class($tokens, $orig_class_name, $new_class_name) {
    $i = 0;
    $new_tokens = [];
    foreach($tokens as $token) {
        $is_class = is_class_instantiation($tokens, $i);
        if($is_class && $token[1] === $orig_class_name) {
            $new_token = [T_STRING, $new_class_name, $token[2]];
            array_push($new_tokens, $new_token);
        } else {
            array_push($new_tokens, $token);
        }
        $i++;
    }

    return $new_tokens;
}

function tokens_to_debug($tokens) {
    $result = array();
    foreach($tokens as $token) {
        if(is_string($token)) {
            array_push($result, $token);
        } else {
            array_push($result, array(token_name($token[0]), $token[1]));
        }
    }

    return $result;
}

function token_to_debug($token) {
    if(is_string($token)) {
        return $token;
    }

    return array(token_name($token[0]), $token[1]);
}

function tokens_to_string($tokens) {
    $result = "";
    foreach($tokens as $token) {
        if(is_string($token)) {
            $result .= $token;
        } else {
            $result .= $token[1];
        }
    }

    return $result;
}

/**
 * Converts param tokens into PhpocParam.
 * @param array $param_tokens
 * @return array
 */
function param_tokens_process($param_tokens) {
    $params = [];
    $pos = 0;
    $next_by_reference = FALSE;
    foreach($param_tokens as $token) {
        if($token[0] === T_VARIABLE) {
            $param_name = ltrim($token[1], '$');
            $param = new \phpoc\PhpocParam($param_name, $pos, $next_by_reference);
            array_push($params, $param);
            $pos++;
            if($next_by_reference) {
                $next_by_reference = FALSE;    
            } 
        } else if($token === "&") {
            $next_by_reference = TRUE;
        }
    }

    return $params;
}

/**
 * Calls to require bring everything to a halt. This code replaces those with
 * include calls which go on.
 */
function replace_requires($body_tokens) {
    $new_tokens = [];
    foreach($body_tokens as $token) {
        if($token[0] === T_REQUIRE || $token[0] === T_REQUIRE_ONCE) {
            $new_token = [T_INCLUDE, "include"];
            array_push($new_tokens, $new_token);
        } else {
            array_push($new_tokens, $token);
        }
    }

    return $new_tokens;
}

function is_allowed_func_token($token) {
    return is_string($token) || $token[0] == T_WHITESPACE;
}

/**
 * Returns whether the current function is a function name. It returns false
 * when invoking inaccessible methods in an object context, as that will be
 * handled elsewhere.
 */
function is_standalone_function_call($body_tokens, $i) {
    $token = $body_tokens[$i];
    $is_function = FALSE;
    if($token[0] === T_STRING) {
        $x = $i - 1;
        $is_object_call = FALSE;
        $is_classname = FALSE;
        $is_static_call = FALSE;
        while($prev_token = @$body_tokens[$x]) {
            if($prev_token[0] === T_OBJECT_OPERATOR) {
                $is_object_call = TRUE;
                break;
            } else if($prev_token[0] === T_NEW) {
                $is_classname = TRUE;
                break;
            } else if($prev_token[0] === T_DOUBLE_COLON) {
                $is_static_call = TRUE;
            }

            if(!is_allowed_func_token($prev_token)) {
                break;
            }
            $x--;
        }

        if(!$is_object_call && !$is_classname && !$is_static_call) {
            $x = $i + 1;
            while($next_token = @$body_tokens[$x]) {
                if($next_token === "(") {
                    $is_function = TRUE;
                    break;
                } 

                if(!is_allowed_func_token($next_token)) {
                    break;
                }
                $x++;
            }
        }
    }

    return $is_function;
}

/**
 * Determines if T_STRING belongs to class instantiation.
 * @param $tokens list of tokens.
 * @param $i position of particular token.
 */
function is_class_instantiation($tokens, $i) {
    $token = $tokens[$i];
    if($token[0] === T_STRING) {
        $x = $i - 1;
        while(@$prev_token = $tokens[$x]) {
            if($prev_token[0] === T_NEW) {
                return TRUE;
            }

            if(!is_allowed_func_token($prev_token)) {
                break;
            }

            $x--;
        }
    }

    return FALSE;
}

/**
 * Returns true if token is a static class call, e.g. Classname::static_method();
 */
function is_static_class_call($tokens, $i) {
    $token = $tokens[$i];
    if($token[0] === T_STRING) {
        $x = $i + 1;
        while(@$next_token = $tokens[$x]) {
            if($next_token[0] == T_DOUBLE_COLON) {
                return TRUE;
            }

            if(!is_allowed_func_token($next_token)) {
                break;
            }

            $x++;
        }
    }

    return FALSE;
}

/**
 * Replace missing classes.
 */
function replace_missing_classes($body_tokens) {
    $i = 0;
    $new_tokens = [];
    foreach($body_tokens as $token) {
        $is_class_instantiation = is_class_instantiation($body_tokens, $i);
        $is_static_call = is_static_class_call($body_tokens, $i);

        if($is_class_instantiation || $is_static_call) {
            if(!class_exists($token[1])) {
                $new_token = [T_STRING, "\\phpoc\\ObjectMock", $token[2]];
                array_push($new_tokens, $new_token);
            } else {
                array_push($new_tokens, $token);
            }
        } else {
            array_push($new_tokens, $token);
        }
        $i++;
    }

    return $new_tokens;
} 

/**
 * Replace missing functions with void function which exists.
 * @param array $body_tokens the tokens for the function.
 * @param array $override functions which will later be overriden. These will
 * not be replaced.
 */
function replace_missing_functions($body_tokens, $override) {
    $new_tokens = [];
    for($i = 0; $i < sizeof($body_tokens); $i++) {
        $token = $body_tokens[$i];
        if($token[0] === T_STRING) {
            $is_function_name = is_standalone_function_call($body_tokens, $i);
            if($is_function_name) {
                $function_name = $token[1];
                $will_override = array_key_exists($function_name, $override);
                if(function_exists($function_name) || $will_override) {
                    array_push($new_tokens, $token);
                } else {
                    $new_token = array(T_STRING, "\\phpoc\\parser\\function_missing", $token[2]); 
                    array_push($new_tokens, $new_token);
                }
            } else {
                array_push($new_tokens, $token);
            }
        } else {
            array_push($new_tokens, $token);
        }
    }

    return $new_tokens;
}

/**
 * Convenience function for getting the tokens of a string of php code without
 * having to prepend "<?php".
 * @return array the tokens for the string provided.
 */
function tokens_get($string) {
    $tokens = token_get_all("<?php ".$string);
    return array_slice($tokens, 1);
}

/**
 * Replace $this with a stub.
 */
function replace_this($body_tokens) {
    $contains_this = FALSE;
    foreach($body_tokens as $token) {
        if(is_array($token) && $token[1] === '$this') {
            $contains_this = TRUE;
            break;
        }
    }

    if(TRUE === $contains_this) {
        $this_name = '$this_' . \phpoc\uid_get();
        $create_mock_tokens = tokens_get($this_name . ' = new \\phpoc\\ObjectMock();');

        $new_tokens = $create_mock_tokens; 
        foreach($body_tokens as $token) {
            if(is_array($token) && $token[1] === '$this') {
                $token[1] = $this_name;
            }
            array_push($new_tokens, $token);
        }

        return $new_tokens;
    } else {
        return $body_tokens;
    }
}

/**
 * Replace calls to exit and die with calls to the echo construct, to prevent
 * the program from exiting.
 */
function replace_exits($body_tokens) {
    $new_tokens = [];
    $deleting = FALSE;
    foreach($body_tokens as $token) {
        if(is_array($token) && $token[0] === T_EXIT) {
            $deleting = TRUE;
        } 

        if(!$deleting) { 
            array_push($new_tokens, $token);
        }

        if($token === ";") {
            $deleting = FALSE;
        }
    }

    return $new_tokens;
}

/**
 * Remove calls to exit.
 */
function replace_sleeps($body_tokens) {
    $exit_funcs = [
        'sleep',
        'usleep',
        'time_sleep_until',
        'time_nanosleep'
    ];

    $new_tokens = [];
    $deleting = FALSE;
    foreach($body_tokens as $token) {
        if(is_array($token) && $token[0] == T_STRING && in_array($token[1], $exit_funcs)) {
            $deleting = TRUE;
        }

        if(!$deleting) {
            array_push($new_tokens, $token);
        }

        if($token === ";") {
            $deleting = FALSE;
        }
    }

    return $new_tokens;
}

/**
 * Returns an array of strings, each being a global in use within the function.
 */
function globals_get($function) {
    $globals = [];
    $parsing_globals = FALSE;
    foreach($function->body_tokens as $token) {
        if(is_array($token) && $token[0] === T_GLOBAL) {
            $parsing_globals = TRUE;
        }

        if($parsing_globals) {
            if(is_array($token) && $token[0] === T_VARIABLE) {
                array_push($globals, substr($token[1], 1));
            }

            if($token === ";") {
                $parsing_globals = FALSE;
            }
        }
    }

    return $globals;
}
