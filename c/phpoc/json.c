#include <stdio.h>

#include "lib/json-builder/json-builder.h"
#include "lib/sds/sds.h"
#include "phpoc/analyse.h"
#include "phpoc/dbg.h"
#include "phpoc/list.h"
#include "phpoc/json.h"

int parameters_parse(json_value *params_json, List *parameters) {
    check(params_json->type == json_array, "Expected array");

    int i;
    for(i = 0; i < params_json->u.array.length; i++) {
        json_value *val = params_json->u.array.values[i];
        check(val->type, "Expected only strings.");
        sds val_str = sdsnew(val->u.string.ptr);
        list_push(parameters, val_str);
    }

    return 0;

error:
    return -1;
}

int call_parse(json_value *call_json, DangerousCall *call) {
    check(call_json->type == json_object, "Expected call.");

    int i;
    for(i = 0; i < call_json->u.object.length; i++) {
        char *key = call_json->u.object.values[i].name;
        json_value *val = call_json->u.object.values[i].value;

        if(val->type == json_string) {
            char *val_str = val->u.string.ptr;
            if(strcmp(key, "function_name") == 0) {
                call->function_name = sdsnew(val_str);
            } else if(strcmp(key, "sink_called_with")) {
                call->sink_called_with = sdsnew(val_str);
            }
        } else {
            if(strcmp(key, "parent_parameters") == 0) {
                List *parent_parameters = list_create();
                int res = parameters_parse(val, parent_parameters);
                check(res == 0, "Error parsing param.");
                call->parent_parameters = parent_parameters;
            }
        }
    }

    return 0;

error:
    return -1;
}

int calls_parse_list(json_value *calls_list_json, List *calls) {
    check(calls_list_json->type == json_array, "Expected array");

    int i;
    for(i = 0; i < calls_list_json->u.array.length; i++) {
        json_value *obj_parsed = calls_list_json->u.array.values[i];

        DangerousCall *call = calloc(1, sizeof(DangerousCall));
        int res = call_parse(obj_parsed, call);
        check(res == 0, "Error parsing call.");
        list_push(calls, call);
    }

    return 0;

error:
    return -1;
}

int sink_parse(json_value *sink_json, PhpocSink *sink) {
    check(sink_json->type == json_object, "Expected a sink.");

    int i;
    for(i = 0; i < sink_json->u.object.length; i++) {
        char *key = sink_json->u.object.values[i].name;
        json_value *val = sink_json->u.object.values[i].value;

        if(strcmp("function_name", key) == 0) {
            sink->function_name = sdsnew(val->u.string.ptr);
        } else if(strcmp("param_no", key) == 0) {
            sink->param_no = val->u.integer;
        } else if(strcmp("calls", key) == 0) {
            List *calls = list_create();
            calls_parse_list(val, calls);
            sink->calls = calls;
        }
    }

    return 0;

error:
    return -1;
}

int sinks_parse_list(json_value *sinks_file_json, List *sinks) {
    check(sinks_file_json->type == json_array, "Expected array");

    int i;
    for(i = 0; i < sinks_file_json->u.array.length; i++) {
        json_value *obj_parsed = sinks_file_json->u.array.values[i];

        PhpocSink *sink = calloc(1, sizeof(PhpocSink));
        int res = sink_parse(obj_parsed, sink);
        check(res == 0, "Error parsing sink");
        list_push(sinks, sink);
    }

    return 0;
error:
    return -1;
} 

int _json_parse_file(json_value *file_json, PhpocFile *file) {
    check(file_json->type == json_object, "Expected only files.");

    int i;
    for(i = 0; i < file_json->u.object.length; i++) {
        char *key = file_json->u.object.values[i].name;
        json_value *val = file_json->u.object.values[i].value;
        if(strcmp("path", key) == 0) {
            file->path = sdsnew(val->u.string.ptr);
        } else if(strcmp("sinks", key) == 0) {
            List *sinks = list_create();
            int res = sinks_parse_list(val, sinks);
            check(res == 0, "Error parsing sinks");
            file->sinks = sinks;
        }
    }

    return 0;

error:
    return -1;
}

int json_parse_file(sds json_str, PhpocFile **file) {
    json_value *obj_parsed = json_parse(json_str, sdslen(json_str));
    check(obj_parsed != NULL, "Error parsing json string");

    *file = calloc(1, sizeof(PhpocFile));
    int res = _json_parse_file(obj_parsed, *file);
    json_value_free(obj_parsed);

    check(res == 0, "Error parsing object");

    return 0;
error:
    return -1;
}

/**
 * Obtains the result from the parsed JSON RPC response.
 */
int rpc_result(json_value *parsed, json_value **result) {
    json_obj_foreach(parsed) {
        json_obj_get(key, val);
        if(strcmp("result", key) == 0) {
            *result = val;
            break;
        }
    }

    if(result == NULL) {
        return -1;
    } else {
        return 0;
    }

error:
    return -1;
}

/**
 * Parses a JSON RPC response which contains a list of strings in the result
 * body.
 * E.g. {"result":["lel","lol","lalalala"],"error":null}
 */
int json_parse_rpc_list(sds rpc_response, List *l) {
    json_value *parsed = json_parse(rpc_response, sdslen(rpc_response));
    check(parsed != NULL, "Error parsing json string");

    json_value *result_array = NULL;
    int res = rpc_result(parsed, &result_array);
    check(res == 0, "Error parsing.");

    json_arr_foreach(result_array) {
        json_arr_get(val);
        sds val_str = sdsnew(val->u.string.ptr);
        list_push(l, val_str);
    }

    json_value_free(parsed);

    return 0;

error:
    return -1;
}
