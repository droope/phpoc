
#include <unistd.h>

#include "lib/json-builder/json-builder.h"
#include "lib/sds/sds.h"
#include "phpoc/analyse.h"
#include "phpoc/dbg.h"
#include "phpoc/list.h"
#include "phpoc/util.h"

void rpc_call_generate(char *method, List *params, sds *out_json) {
    json_value *rpc_method = json_string_new(method);
    json_value *rpc_params;
    if(params != NULL) {
        rpc_params = json_array_new(params->count);
        list_foreach(params, arg_li) {
            json_value *arg = json_string_new(arg_li->value);
            json_array_push(rpc_params, arg);
        }
    } else {
        rpc_params = json_array_new(0);
    }

    json_value *rpc_call = json_object_new(2);
    json_object_push(rpc_call, "method", rpc_method);
    json_object_push(rpc_call, "params", rpc_params);

    char *buf = malloc(json_measure(rpc_call));
    json_serialize(buf, rpc_call);

    *out_json = sdsnew(buf);

    free(buf);
    json_builder_free(rpc_call);
}

int rpc_call_sds(sds rpc_json, int timeout, sds *out) {
    char *cmd[] = {"/usr/bin/php", "php/phpoc.php", "rpc", NULL};
    int infp = 0, outfp = 0, errfp = 0;

    pid_t pid = phpoc_popen(cmd, &infp, &outfp, &errfp);
    check(pid != -1, "RPC command failed.");
    
    write(infp, rpc_json, sdslen(rpc_json));
    close(infp);

    int call_res = phpoc_pread_timeout(pid, outfp, timeout, out);
    debug("%s", *out);

    int timed_out = call_res == -1;
    int subprocess_err = call_res > 0;
    if(timed_out) {
        log_err("RPC call timed out");
        goto error;
    } else if(subprocess_err) {
#ifdef NDEBUG
        log_err("RPC call failed with error code: %d", call_res);
        goto error;
#else
        sds err;
        phpoc_read_timeout(errfp, timeout, &err);
        log_err("RPC call failed with error: %s", err);
        sdsfree(err);
#endif
    }

    return 0;

error:
    return -1;
}

/**
 * Calls a PHP RPC method.
 * Returns -1 on failure, 0 on success.
 */
int rpc_call(char *method, List *params, int timeout, sds *out) {
    sds rpc_json;
    rpc_call_generate(method, params, &rpc_json);
    debug("RPC call: %s", rpc_json);

    int res = rpc_call_sds(rpc_json, timeout, out);

    sdsfree(rpc_json);

    return res;
}

void sinks_get(List *file_sinks, List *sinks) {
    list_foreach(file_sinks, file_it) {
        PhpocFile *file = file_it->value;
        list_foreach(file->sinks, sinks_it) {
            PhpocSink *sink = sinks_it->value;
            list_push(sinks, sink);
        }
    }
}

void rpc_overrides_generate(List *file_sinks, sds *out_json) {
    List *sinks = list_create();
    sinks_get(file_sinks, sinks); 

    int overrides_len = sinks->count;
    json_value *overrides = json_object_new(overrides_len);

    list_foreach(sinks, sink_it) {
        PhpocSink *sink = sink_it->value;
        json_value *tmp_arr = json_array_new(2);

        list_foreach(sink->calls, call_it) {
            DangerousCall *call = call_it->value;
            json_value *func_name = json_string_new(call->function_name);
            json_value *param_no = json_integer_new(sink->param_no);

            json_array_push(tmp_arr, func_name);
            json_array_push(tmp_arr, param_no);

            break;
        }

        json_object_push(overrides, sink->function_name, tmp_arr);
    }
    
    char *json_str = malloc(json_measure(overrides));
    json_serialize(json_str, overrides);

    *out_json = sdsnew(json_str);

    list_destroy(sinks);
    json_builder_free(overrides);
    free(json_str);
}
