<?php

namespace phpoc;

/**
 * API for a scanned PHP file.
 */
class PhpocFile {
    public $path;
    public $sinks;

    public function __construct($path, $sinks) {
        $this->path = $path;
        $this->sinks = $sinks;
    }
}

/**
 * API for a dangerous call.
 */
class DangerousCall {
    /**
     * Name of the dangerous function called.
     */
    public $function_name;

    /**
     * Parameter value which resulted in the dangerous call. This is an array
     * which includes all parameters passed to the function above..
     */
    public $parent_parameters;

    /**
     * Value the dangerous function was called with.
     */
    public $sink_called_with;

    public function __construct($function_name, $parent_parameters, $sink_called_with) {
        $this->function_name = $function_name;
        $this->parent_parameters = $parent_parameters;
        $this->sink_called_with = $sink_called_with;
    }
}

/**
 * Represents a call to a standalone function, a method within a class or a
 * call to a mocked global variable.
 */
class FunctionCall {
    public $function_name;
    public $class_name = NULL;
    public $global_name = NULL;
    public $call_args;
}

/**
 * API for a function which, when called with certain parameters, results in a
 * dangerous call.
 */
class Sink {
    /**
     * Name of the function.
     */
    public $function_name;

    /**
     * Dangerous parameter location, a 0 based integer.
     */
    public $param_no;

    /**
     * An array of DangerousCall objects.
     */
    public $calls;

    public function __construct($function_name, $param_no, $calls) {
        $this->function_name = $function_name;
        $this->param_no = $param_no;
        $this->calls = $calls;
    }
}

class PhpocParam {
    public $pos;
    public $name;
    public $pass_by_reference;

    public function __construct($name, $pos, $pass_by_reference) {
        $this->name = $name;
        $this->pos = $pos;
        $this->pass_by_reference = $pass_by_reference;
    }
}

class JsonRPCResponse {
    public $result;
    public $error;

    public function __construct($result, $error = NULL) {
        if($result && $error) {
            throw new Exception("Both result and error set.");
        }

        $this->result = $result;
        $this->error = $error;
    }
}

class PhpocFunction {
    public $name;
    public $tokens = array();
    public $body_tokens = array();
    public $param_tokens = array();

    /**
     * Converts the body tokens in this class into their string representation
     * @return string
     */
    public function get_body_str() {
        return $this->tokens_to_string($this->body_tokens);
    }

    public function get_params_str() {
        return $this->tokens_to_string($this->param_tokens);
    }

    public function get_params() {
        return \phpoc\parser\param_tokens_process($this->param_tokens);
    }

    private function tokens_to_string($tokens) {
        return \phpoc\parser\tokens_to_string($tokens);
    }

    public function tokens_to_debug($tokens) {
        return \phpoc\parser\tokens_to_debug($tokens);
    }

    /**
     * Executes the function. All parameters passed to this function will be
     * passed to the original function.
     * @return void
     */
    public function exec() {
        $call_args = func_get_args();
        $func = create_function($this->get_params_str(), $this->get_body_str());
        return call_user_func_array($func, $call_args);
    }

    /**
     * As above, but takes an array with the parameters.
     * @param array $call_args the parameters to call the function with.
     */
    public function exec_array($call_args) {

        \phpoc\debug($this->name . "(" . implode(',', $call_args) . ")");
        \phpoc\debug($this->get_body_str());

        $this->process_references($call_args, $this->get_params());
        $func = create_function($this->get_params_str(), $this->get_body_str());
        $ret = call_user_func_array($func, $call_args);

        return $ret;
    }

    public function __debugInfo() {
        return array($this->name, $this->tokens_to_debug($this->body_tokens));
    }

    public function __toString() {
        return $this->tokens_to_string($this->tokens);
    }

    public function replace_function($orig_func_name, $new_func_name) {
        $this->body_tokens = \phpoc\parser\replace_function($this->body_tokens,
            $orig_func_name, $new_func_name);
    }

    public function replace_class($orig_class_name, $new_class_name) {
        $this->body_tokens = \phpoc\parser\replace_class($this->body_tokens,
            $orig_class_name, $new_class_name);
    }

    /**
     * Replaces require_once's etc.
     */
    public function preprocess($overrides = NULL) {
        $this->body_tokens = \phpoc\parser\replace_requires($this->body_tokens);
        $this->body_tokens = \phpoc\parser\replace_missing_functions($this->body_tokens, $overrides);
        $this->body_tokens = \phpoc\parser\replace_this($this->body_tokens);
        $this->body_tokens = \phpoc\parser\replace_missing_classes($this->body_tokens);
        $this->body_tokens = \phpoc\parser\replace_exits($this->body_tokens);
        $this->body_tokens = \phpoc\parser\replace_sleeps($this->body_tokens);
    }

    /**
     * Convert values into references when required.
     */
    private function process_references(&$call_args, $params) {
        foreach($params as $key => $param) {
            if($param->pass_by_reference) {
                $call_args[$key] = &$call_args[$key];
            } else {
                $call_args[$key] = $call_args[$key];
            }
        }
    } 
}

class EventManager {

    private static $hooks = array();

    private static function getCallbacks($name) {
        return isset(self::$hooks[$name]) ? self::$hooks[$name] : array();
    }

    public static function add($name, $callback) {
        if (!is_callable($callback, true)) {
            throw new \InvalidArgumentException(sprintf('Invalid callback: %s.', print_r($callback, true)));
        }

        self::$hooks[$name][] = $callback;
    }

    public static function fire($name, $args) {
        foreach(self::getCallbacks($name) as $callback) {
            call_user_func($callback, $args);
        }
    }
}

class PhpocMock {

    public $name;
    public $overriden_method;
    public $call_args_array;

    /**
     * Mocks a function. Records calls and arguments.
     * @param string $name the name of the function to mock. E.g. mysql_query.
     */
    public function __construct($name) {
        $this->name = $name;
        $this->reset();
    }

    /**
     * Applies the mock to a function
     * @param PhpocFunction $function the function.
     */
    public function apply($function) {
        $new_func_name = "phpoc_mock_" . uid_get();
        $function->replace_function($this->name, $new_func_name);
        EventManager::add($new_func_name, array($this, "called"));
        eval("function $new_func_name() {\\phpoc\\EventManager::fire('$new_func_name', func_get_args());}");
    }

    /**
     * Triggered when the mocked function is called.
     * @param array $params the parameters passed to the function.
     */
    public function called($params) {
        array_push($this->call_args_array, $params);
    }

    public function reset() {
        $this->call_args_array = array();
    }

}

class PhpocClassMock {

    public $name;
    public $call_args_array;

    /**
     * Mocks a function. Records calls and arguments.
     * @param string $name the name of the class to mock. E.g. mysqli.
     */
    public function __construct($name) {
        $this->name = $name;
        $this->reset();
    }

    /**
     * Applies the mock to a function
     * @param PhpocFunction $function the function.
     */
    public function apply($function) {
        $new_class_name = "phpoc_class_" . uid_get();
        $function->replace_class($this->name, $new_class_name);

        EventManager::add($new_class_name, array($this, "called"));
        eval("class $new_class_name extends \\phpoc\\ObjectMock { 
            function __call(\$name, \$arguments) {\\phpoc\\EventManager::fire('$new_class_name', func_get_args());
            return new \\phpoc\\ObjectMock(); }}");
    }

    /**
     * Triggered when the mocked function is called.
     * @param array $params the parameters passed to the function.
     */
    public function called($func_get_args) {
        list($method_called, $args) = $func_get_args;
        if(@!$this->call_args_array[$method_called]) {
            $this->call_args_array[$method_called] = [];
        }

        array_push($this->call_args_array[$method_called], $args);
    }

    public function reset() {
        $this->call_args_array = [];
    }
}

class PhpocGlobalMockUtil {
    public $global_id = "";
    
    function __construct($global_id) {
        $this->global_id = $global_id;
    }

    function __call($name, $arguments) {
        \phpoc\EventManager::fire($this->global_id, func_get_args());
        return new \phpoc\ObjectMock();
    }
}

class PhpocGlobalMock extends PhpocClassMock {
    public $name;
    public $call_args_array;

    public function __construct($name) {
        $this->name = $name;
        $this->reset();
    }

    /**
     * Applies the mock to a global variable.
     */
    public function apply() {
        $global_id = "phpoc_global_" . uid_get();
        $GLOBALS[$this->name] = new PhpocGlobalMockUtil($global_id);
        EventManager::add($global_id, array($this, "called"));
    }

    public function reset() {
        $this->call_args_array = [];
    }
}

/**
 * Missing objects are replaced with this. This is a versatile object which
 * goes out of it's way to prevent fatal errors when interacted with.
 */
class ObjectMock extends \ArrayObject {

    private $internalCounter = 0;

    function __construct() {
    }

    /**
     * When called, return another mock unless called too many times, to
     * prevent infinite while loops.
     */
    function __call($name, $arguments) {
        //if($this->internalCounter < 30) {
            //$this->internalCounter++;
            //return new ObjectMock();
        //} else {
            return NULL;
        //}
    }

    function __get($name) {
        //return new ObjectMock();
        return NULL;
    }

    function __toString() {
        return "";
    }

    static function __callStatic($name, $arguments) {
        return NULL;
    }
}
