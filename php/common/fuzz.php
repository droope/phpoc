<?php

namespace phpoc\fuzzer;

require_once "common/include.php";

if(!\phpoc\GlobalOptions::get('debug')) {
    $prev_error_rep = error_reporting();
    error_reporting(E_ERROR | E_PARSE | E_COMPILE_ERROR | E_RECOVERABLE_ERROR);
}

/**
 * Fuzzes function in directory, recursing.
 * @param $dir the directory to fuzz.
 */
function fuzz_dir($dir) {
    $di = new \RecursiveDirectoryIterator($dir);
    $files = [];
    foreach (new \RecursiveIteratorIterator($di) as $file => $file_obj) {
        if(\phpoc\ends_with($file, '.php')) {
            $sinks = fuzz($file);
            $f = new \phpoc\PhpocFile($file, $sinks);
            array_push($files, $f);
        }
    }

    return $files;
}

/**
 * Fuzzes functions in file.
 * @param string $file file to fuzz.
 * @param array $overrides functions to override.
 * @return array sinks found.
 */
function fuzz($file, $override = NULL) {
    \phpoc\debug("Fuzzing file $file.");

    $functions = \phpoc\parser\functions_get($file);

    $sinks = [];
    foreach($functions as $function) {
        $function_sinks = function_fuzz($function, $override = NULL);
        if(sizeof($function_sinks) > 0) {
            $function_sinks = $function_sinks;
            $sinks = array_merge($sinks, $function_sinks);
        }
    }

    return $sinks;
}

/**
 * Fuzzes a function and returns a list of all dangerous functionality that
 * gets called without sanitising user input.
 * @param PhpocFunction $function
 * @param array $override functions to override array for recursive tests.
 * @return array sinks found
 */
function function_fuzz($function_orig, $override = NULL) {
    \phpoc\debug("Fuzzing function {$function_orig->name}.");
    $func_processed = clone $function_orig;
    $func_processed->preprocess($override);

    $fuzzers = fuzzers_get_all();
    $sinks = [];
    foreach($fuzzers as $fuzzer_class) {
        $fuzzer = new $fuzzer_class;
        $function = clone $func_processed;
        $params = $function->get_params();
        $mocks = mocks_apply($fuzzer, $function, $override);

        foreach($params as $param_no => $param) {
            $param_id = param_fuzz($params, $param_no, $fuzzer, $function, $mocks);
        }

        $fuzzer_sinks = sinks_get($fuzzer, $function_orig);
        $sinks = array_merge($sinks, $fuzzer_sinks);
    }

    return $sinks;
}

/**
 * Handles the fuzzing of a single parameter. 
 */
function param_fuzz($params, $param_no, $fuzzer, $function, $mocks) {
    $param = $params[$param_no];
    $placeholder_other = "placeholder_str";
    $args = array_fill(0, sizeof($params), $placeholder_other);
    while(NULL !== $fuzz_round = $fuzzer->fuzz($param_no)) {
        $fuzz_string = $fuzz_round[0];
        $process_func_name = $fuzz_round[1];

        $args[$param->pos] = $fuzz_string;
        $function->exec_array($args);

        $calls = calls_get($mocks);
        call_user_func(array($fuzzer, $process_func_name), $param_no, $calls, $args);

        mocks_reset($mocks);
    }
}

function sinks_get($fuzzer, $function) {
    $sinks = [];
    foreach($fuzzer->dangerous_calls_get() as $param_no => $dc) {
        $sink = new \phpoc\Sink($function->name, $param_no, $dc);
        array_push($sinks, $sink);
    }

    return $sinks;
}

/**
 * Applies all mocked to the function object.
 * @param PhpocFuzzer $fuzzer 
 * @param PhpocFunction $function
 */
function mocks_apply($fuzzer, $function, $override) {
    $function_mocks = mock_functions($fuzzer, $function, $override);
    $class_mocks = mock_classes($fuzzer, $function);
    $global_mocks = mock_globals($fuzzer, $function);

    return array_merge($function_mocks, $class_mocks, $global_mocks);
}

function mock_globals($fuzzer, $function) {
    $mocks = [];
    $globals = \phpoc\parser\globals_get($function);
    foreach($globals as $global) {
        $mock = new \phpoc\PhpocGlobalMock($global);
        $mock->apply();
        array_push($mocks, $mock);
    } 

    return $mocks;
}

function mock_classes($fuzzer, $function) {
    $mocks = [];
    foreach($fuzzer->class_mocks_required() as $mock_str) {
        $mock = new \phpoc\PhpocClassMock($mock_str);
        $mock->apply($function);
        array_push($mocks, $mock);
    }

    return $mocks;
}

function mock_create($func, $override) {
    $override_func_name = array_search($func, $override);
    $mock = new \phpoc\PhpocMock();

    if($override_func_name !== NULL) {
        $mock->name = $override_func_name;
        $mock->overriden_method = $func;
    } else {
        $mock->name = $func;
    }

    return $mock;
}

function mock_functions($fuzzer, $function, $override) {
    $mocks = [];
    foreach($fuzzer->mocks_required() as $mock_str) {
        $has_wildcard = \phpoc\str_contains($mock_str, "*");
        if($has_wildcard) {
            $functions = get_defined_functions();
            foreach($functions['internal'] as $func) {
                if(fnmatch($mock_str, $func)) {
                    $mock = mock_create($func, $override);

                    $mock->apply($function);
                    array_push($mocks, $mock);
                }
            }
        } else {
            $mock = mock_create($mock_str, $override);

            $mock->apply($function);
            array_push($mocks, $mock);
        }
    }

    return $mocks;
}

/**
 * Deletes all recorded calls from all mocks.
 * @param array $mocks the mocks to reset.
 */
function mocks_reset($mocks) {
    foreach($mocks as $mock) {
        $mock->reset();
    }
}

/**
 * Returns all the calls received in the mocks.
 */
function calls_get($mocks) {
    $calls = [];
    foreach($mocks as $mock) {
        $mock_type = get_class($mock);
        $is_class = \phpoc\ends_with($mock_type,"PhpocClassMock");
        $is_global = \phpoc\ends_with($mock_type, "PhpocGlobalMock");
        foreach($mock->call_args_array as $key => $elem) {
            $call = NULL;
            if(\phpoc\ends_with($mock_type, "PhpocMock")) {
                $call = new \phpoc\FunctionCall();
                $call->function_name = $mock->name;
                $call->overriden_method = $mock->overriden_method;
                $call->call_args = $elem;

                array_push($calls, $call);
            } else if($is_class || $is_global) {
                $function_name = $key;
                foreach($elem as $call_args) {
                    $call = new \phpoc\FunctionCall();
                    $call->function_name = $function_name;
                    $call->call_args = $call_args;

                    if($is_class) {
                        $call->is_class = TRUE;
                        $call->class_name = $mock->name;
                    } else {
                        $call->is_global = TRUE;
                        $call->global_name = $mock->name;
                    }

                    array_push($calls, $call);
                }
            } else {
                throw new \Exception("Unhandled mock type '${mock_type}'.");
            }
        }
    }

    return $calls;
}

/**
 * Returns all fuzzers by iterating on all known classes and searching for
 * those which start with "phpdoc\fuzzer" and end in "Fuzzer".
 */
function fuzzers_get_all() {
    $all_classes = get_declared_classes();
    $fuzzer_classes = [];
    foreach($all_classes as $class) {
        if(\phpoc\starts_with($class, "phpoc\\fuzzer") &&
                \phpoc\ends_with($class, "Fuzzer")) {
            array_push($fuzzer_classes, $class);
        }
    }

    return $fuzzer_classes;
}
