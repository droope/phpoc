#define _GNU_SOURCE
#include <ctype.h>
#include <fcntl.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <time.h>
#include <unistd.h>

#include "phpoc/dbg.h"
#include "phpoc/util.h"
#include "lib/sds/sds.h"

void display_help() {
    fprintf(stderr, "Usage: phpoc target\n");
}

struct Options *opts_parse(int argc, char *argv[]) {
    struct Options *opts = malloc(sizeof(struct Options));
    int help = 0;
    int index = 0;
    int c = 0;
    opterr = 0;
    while ((c = getopt (argc, argv, "h")) != -1) {
        switch (c) {
            case 'h':
                help = 1;
                break;
            case '?':
                if (isprint (optopt)) {
                    fprintf (stderr, "Unknown option `-%c'.\n", optopt);
                } else {
                    fprintf (stderr, "Unknown option character `\\x%x'.\n\n", optopt);
                }

                goto error;
            default:
                abort ();
        }
    }

    if(help == 1) {
        goto error;
    }

    char *target;
    int target_defined = 0;
    for (index = optind; index < argc; index++) {
        target = argv[index];
        target_defined = 1;
        break;
    }

    if(target_defined) {
        opts->target = sdsnew(target);
    } else {
        fprintf (stderr, "target not specified.\n");
        goto error;
    }

    return opts;

error:
    free(opts);
    return NULL;
}

void opts_free(struct Options *opts) {
    if(opts != NULL) {
        sdsfree(opts->target);
        free(opts);
    }
}

int phpoc_endswith_char(char *str, char c) {
    size_t len = strlen(str);
    return len > 0 && str[len - 1] == c;
}

/**
 * If the char array doesn't end with a /, append one. Used mainly for
 * filenames.
 */
char *phpoc_append_slash(char *str) {
    sds new_str = sdsnew(str);
    if(!phpoc_endswith_char(str, '/')) {
        new_str = sdscat(new_str, "/");
    }

    return new_str;
}

int phpoc_endswith(char *haystack, char *needle) {
    size_t haystack_len = strlen(haystack);
    size_t needle_len = strlen(needle);

    check(needle_len < haystack_len, "needle_len > haystack_len");

    char *last_bit = haystack + (haystack_len - needle_len);
    return strcmp(last_bit, needle) == 0;
error:
    return 0;
}

void get_rel_stream(int *fp, int pipe[2], int pos) {
    if(fp == NULL) {
        close(pipe[STDIN_FILENO]);
        close(pipe[STDOUT_FILENO]);
    } else {
        int closePos = pos == 0 ? 1 : 0;
        close(pipe[closePos]);
        *fp = pipe[pos];
        fcntl(*fp, F_SETFL, O_NONBLOCK);
    }
}

/**
 * Executes **command and populates several file descriptors passed as parameters
 * if said pointers are not null.  *infp corresponds to the subprocess' stdin,
 * *outfp and *errfp correspond to stdout and stderr respectively.
 * O_NONBLOCK is set on all file descriptors.
 */
pid_t phpoc_popen(char **command, int *infp, int *outfp, int *errfp) {
    int p_stdin[2], p_stdout[2], p_stderr[2];
    int pid;

    check(pipe2(p_stdin, O_CLOEXEC) == 0, "Could not create pipe.");
    check(pipe2(p_stdout, O_CLOEXEC) == 0, "Could not create pipe.");

    int errfp_req = errfp != NULL;
    if(errfp_req) {
        check(pipe2(p_stderr, O_CLOEXEC) == 0, "Could not create pipe.");
    }

    pid = fork();
    check(pid >= 0, "Could not create child.");

    if(pid == 0) {
        close(p_stdin[1]);
        close(p_stdout[0]);

        check(dup2(p_stdin[0], STDIN_FILENO) != -1, "Could not dup fd.");
        check(dup2(p_stdout[1], STDOUT_FILENO) != -1, "Could not dup fd.");

        if(errfp_req) {
            close(p_stderr[0]);
            check(dup2(p_stderr[1], STDERR_FILENO) != -1, "Could not dup fd.");
        }

        execvp(*command, command);
        perror("execvp");
        exit(1);
    }

    get_rel_stream(infp, p_stdin, STDOUT_FILENO);
    get_rel_stream(outfp, p_stdout, STDIN_FILENO);
    if(errfp_req) {
        get_rel_stream(errfp, p_stderr, STDIN_FILENO);
    }

    return pid;

error:
    return -1;
}

/**
 * Returns 1 if path is dir, 0 if path is not dir or -1 on error.
 * @param path the path to check.
 */
int phpoc_is_dir(char *path) {
    struct stat path_stat;
    check(stat(path, &path_stat) == 0, "Error reading file '%s'", path);
    return S_ISDIR(path_stat.st_mode);
error:
    return -1;
}

int phpoc_read_file(const char *path, sds *out) {
    *out = sdsempty();

    int fp = open(path, O_RDONLY);
    check(fp >= 0, "Error reading file %s", path);

    char tmpout[1025];
    int tmpout_len = sizeof(tmpout);
    while(1) {
        size_t bytes_read = read(fp, tmpout, tmpout_len - 1);
        check(bytes_read >= 0, "Error reading from file.");

        if(bytes_read == 0) {
            break;
        } else {
            *out = sdscatlen(*out, tmpout, bytes_read);
        }
    }

    close(fp);
    return 0;

error:
    return -1;
}

/**
 * read from *fp with timeout into *out. fp must have been set as O_NONBLOCK.
 * Returns -1 on timeout, or 0 when all data has been read into out.
 */
int phpoc_read_timeout(int fp, int timeout, sds *out) {
    *out = sdsempty();

    clock_t start = time(0);
    double time_spent = 0.0;
    
    char tmpout[1024];
    size_t tmpout_len = sizeof(tmpout);
    int ret_val;
    while(1) {
        ssize_t r = read(fp, tmpout, tmpout_len - 1);
        if(r == -1 && errno == EAGAIN) {
            time_spent = difftime(time(0), start);
            if(time_spent >= timeout) {
                ret_val = -1;
                break;
            } else {
                sleep(1);
                continue;
            }
        } else if (r > 0) {
            *out = sdscatlen(*out, tmpout, r);
        } else {
            ret_val = 0;
            break;
        }
    }

    return ret_val;
}

/**
 * Gets stdout from a running process, and populates the out sds with the
 * contents. out will contain whatever has been read so far,
 * so you still need to free it. In the event of a timeout, the child process
 * will be sent a SIGINT signal.
 * Returns -1 if timeout exceeded, -2 if could not read process exit code, or
 * the command's exit status.
 */
int phpoc_pread_timeout(pid_t pid, int outfp, int timeout, sds *out) {
    int exit_status;
    int rs = phpoc_read_timeout(outfp, timeout, out);
    int read_timeout = rs == -1;

    if(read_timeout) {
        kill(pid, SIGINT);
        exit_status = -1;
    } else {
        int status = 0;
        int wpid_res = waitpid(pid, &status, 0);

        if(wpid_res == -1) {
            exit_status = -2;
        } else {
            exit_status = WEXITSTATUS(status);
        }
    }

    return exit_status;
}
