<?php

namespace phpoc;

define('PHPOC_CWD', dirname(__FILE__) . "/");
set_include_path(PHPOC_CWD);

include "common/include.php";

try {
    $args = new Arguments($argv);
    $controller_file = PHPOC_CWD . "controllers/{$args->controller}.php";
    if(file_exists($controller_file)) {
        require $controller_file;
        $func = "phpoc\\{$args->controller}_init";
        call_user_func_array($func, $args->params);
    } else {
        throw new UserMessageException("Subcommand not found\n");
    }

} catch(UserMessageException $e) {
    echo $e->getMessage();
}
