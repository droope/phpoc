#pragma once

#include <sys/types.h>

#include "lib/sds/sds.h"
#include "phpoc/list.h"

struct ist_args;
typedef struct ist_args {
    List *tasks;
    List *file_sinks;
    pthread_mutex_t tasks_lock;
    pthread_mutex_t sinks_lock;
} ist_args;

struct PhpocFile;
typedef struct PhpocFile {
    sds path;
    List *sinks;
} PhpocFile;

struct PhpocSink;
typedef struct PhpocSink {
    sds function_name;
    int param_no;
    List *calls;
} PhpocSink;

struct DangerousCall;
typedef struct DangerousCall {
    sds function_name;
    sds sink_called_with;
    List *parent_parameters;
} DangerousCall;

int identify_files(List *php_files, char *target_dir, List *required_mocks);
pid_t phpoc_results_call(sds target_file, sds *cmd, int *outfp);
void phpoc_results(sds target_file, PhpocFile **file);
void *identify_sinks_thread(void *args_void);
int identify_file_sinks(const List *php_files, List **file_sinks);
int identify_sinks(char *target, List **file_sinks, sds override);
void required_mocks_get(List *required_mocks);
void phpoc_file_free(PhpocFile *file);
int file_has_patterns(char *filename, List *patterns);
