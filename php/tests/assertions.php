<?php

namespace phpoc\test;

class EnsureException extends \Exception {}

function ensure($bool) {
    if($bool !== TRUE) {
        throw new EnsureException();
    }
}

