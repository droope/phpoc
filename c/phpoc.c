#include <stdio.h>

#include "lib/sds/sds.h"
#include "phpoc/list.h"
#include "phpoc/util.h"
#include "phpoc/analyse.h"

int main(int argc, char *argv[]) {
    List *file_sinks = NULL;
    struct Options *opts = NULL;
    int ret_code = 0;

    opts = opts_parse(argc, argv);
    if(opts == NULL) {
        ret_code = 1;
        display_help();
        goto cleanup;
    }

    if(identify_sinks(opts->target, &file_sinks, NULL) == -1) {
        ret_code = 1;
        goto cleanup;
    }

    list_foreach(file_sinks, cur) {
        PhpocFile *file = cur->value;
        if(file->sinks->count > 0) {
            printf("File %s\n", file->path);
            list_foreach(file->sinks, s_cur) {
                PhpocSink *sink = s_cur->value;
                printf("Function %s is a sink\n", sink->function_name);
            }
        }
        phpoc_file_free(file);
    }

    sds func_override = NULL;

    list_destroy(file_sinks);

cleanup:
    opts_free(opts);

    return ret_code;
}

